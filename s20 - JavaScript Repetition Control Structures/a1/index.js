let num = Number(prompt("Please input a number:"));
console.log("The number you provided is " + num);

for (let i = num; i >= 0; i--){

	if(i <= 50){
		console.log("The value is at 50. Terminating the loop.")
		break;
	} else if(i % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	} else if(i % 5 === 0){
		console.log(i);
	}	
}

let myString = "supercalifragilisticexpialidocious";
let consonants = "";

console.log(myString);
for (let i = 0; i < myString.length; i++){
	if (
		myString[i].toLowerCase() === "a" ||
		myString[i].toLowerCase() === "e" ||
		myString[i].toLowerCase() === "i" ||
		myString[i].toLowerCase() === "o" ||
		myString[i].toLowerCase() === "u"
		){
		
	} else{
		consonants += myString[i];
	}
}
console.log(consonants);

